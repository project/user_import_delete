<?php

/**
 * @file
 * Delete users who are not included in an import file.
 */

/**
 * Implementation of hook_user_import_form_fieldsets().
 * Add options to User Import settings form.
 */
function user_import_delete_user_import_form_fieldset($import, $collapsed) {
  $form = array();
  user_import_delete_user_import_edit_account_cancel($form, $import, $collapsed);
  return $form;
}

/**
 * Implementation of hook_user_import_pre_save().
 * Log users that are in the import file, so we have a list to compare against later.
 * We log them here in case for any reason the account is not created or updated.
 *
 **/
function user_import_delete_user_import_pre_save($settings, $account, $fields, $errors, $update_setting_per_module) {

  $account_key = 'email';

  if (isset($fields['user'][$account_key][0]) && !empty($fields['user'][$account_key][0])) {

    db_delete('user_import_delete')
      ->condition('import_id', $settings['import_id'])
      ->condition('account_key', $fields['user'][$account_key][0])
      ->execute();

    // Add to log
    db_insert('user_import_delete')
      ->fields(array('import_id' => $settings['import_id'], 'account_key' => $fields['user'][$account_key][0]))
      ->execute();
  }

}

/**
 * Implementation of hook_user_import_after_save().
 * Log new or updated accounts, so we have a list to compare against later.
 *
 **/
//function user_import_delete_user_import_after_save($settings, $account, $password, $fields, $updated, $update_setting_per_module) {
//
//  // delete from log first
//
//  // Add to log
//  db_insert('user_import_delete')
//    ->fields(array('import_id' => $settings['import_id'], 'uid' => $account->uid))
//    ->execute();
//
//}

/**
 * Implementation of hook_user_import_imported().
 * Process import once it's completed.
 *
 **/
function user_import_delete_user_import_imported($import_id, $settings) {
  $method = $settings['user_import_delete'];

  if (!empty($method)) {
    user_import_delete_cancel_accounts($import_id, $method);
  }
}

/**
 * Delete users not included in an import.
 *
 **/
function user_import_delete_cancel_accounts($import_id, $method) {
  global $user;
  $edit = array();

  // get accounts not in the log
  $accounts_not_created = db_query('SELECT u.uid FROM {users} u  WHERE u.mail NOT IN (SELECT ud.account_key FROM {user_import_delete} ud WHERE import_id = :import_id)', array(':import_id' => $import_id));

  foreach ($accounts_not_created as $account_info) {
    // Prevent cancelling user 1 or user administrators from deleting themselves without confirmation.
    if ($account_info->uid > 1 && $account_info->uid != $user->uid) {
      $account = user_load($account_info->uid);

      // Initialize batch (to set title).
      $batch = array(
        'title' => t('Cancelling account'),
        'operations' => array(),
      );

      batch_set($batch);

      // Modules use hook_user_delete() to respond to deletion.
      if ($method != 'user_cancel_delete') {
        // Allow modules to add further sets to this batch.
        module_invoke_all('user_cancel', $edit, $account, $method);
      }

      // Finish the batch and actually cancel the account.
      $batch = array(
        'title' => t('Cancelling user account'),
        'operations' => array(
          array('_user_cancel', array($edit, $account, $method)),
        ),
      );

      batch_set($batch);

      db_delete('user_import_delete')
        ->condition('import_id', $import_id)
        ->condition('account_key', $account->mail)
        ->execute();
    }
  }

  batch_process('admin/people/user_import');
  return;
}

/**
 * Options for the User Import settings form.
 */
function user_import_delete_user_import_edit_account_cancel(&$form, $import, $collapsed) {

  $options = array(
    'user_cancel_block' => t('Disable the account and keep its content'),
    'user_cancel_block_unpublish' => t('Disable the account and unpublish its content'),
    'user_cancel_reassign' => t('Delete the account and make its content belong to the !anonymous-name user', array('!anonymous-name' => variable_get('anonymous', t('Anonymous')))),
    'user_cancel_delete' => t('Delete the account and its content'),
  );

  $form['optional']['user_import_delete'] = array(
    '#type'          => 'select',
    '#title'         => t('Existing Accounts Not In Import'),
    '#description'   => t('How to deal with existing accounts that are not in this import.'),
    '#default_value' => isset($import['options']['user_import_delete']) ? $import['options']['user_import_delete'] : '',
    '#empty_option'  => t("Leave active"),
    '#options'       => $options,
  );

}
