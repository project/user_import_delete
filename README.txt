********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: User Import Delete Module
Author: Robert Castelo <www.codepositive.com>
Drupal: 7.x
********************************************************************
DESCRIPTION:

Provides options for cancelling existing accounts that are not in an import.

The typical user case is that a staff list is imported into an intranet every month, and if someone 
has left the organisation they are not included in the staff list, and so their account is cancelled.

The options provided are the same as are available when deleting user accounts individually or
from the People admin page.


********************************************************************
PREREQUISITES:

User Import module required.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire module directory into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes. 



********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/user_import
   
- Commission New Features:
   http://drupal.org/user/3555/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

   http://www.amazon.co.uk/registry/wishlist/4KP50DVHBY62

        
********************************************************************
ACKNOWLEDGEMENT

